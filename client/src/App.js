import React, { useEffect, useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

import HeaderMenu from './component/HeaderMenu/HeaderMenu';
import HeaderSearch from './component/HeaderSearch/HeaderSearch';
import NavbarWapper from './component/Navbar/NavbarWapper';
import ProductWarranty from './component/ProductWarranty/ProductWarranty';
import BestSellerSwiper from './component/BestSellerSwiper/BestSellerSwiper';
import Recommendations from './component/Recommendations/Recommendations';
import AllProducts from './component/AllProducts/AllProducts';
import { Counter } from './component/Counter';


const App = () => {

  const [backendData, setBackendData] = useState([{}])

  useEffect(() => {
    fetch("/api").then(response => response.json()).then(data => {
      setBackendData(data)
    })
  }, [])

  const ShowServer = () => {

    {
      (typeof backendData.users === 'undefined') ? (
        <p>Loading...</p>
      ) : (
        backendData.users.map((user, i) => (
          <p key={i}> {user}</p>
        ))
      )
    }
  }
  return (
    <div>
      <NavbarWapper />
      {/* <Counter/> */}
      <HeaderMenu />
      <HeaderSearch />
      <ProductWarranty/>
      <BestSellerSwiper/>
      <Recommendations/>
      <AllProducts/>
    </div>
  )
}

export default App